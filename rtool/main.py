from . import arguments
from . import config
from . import api
from . import helpers
from . import services
import sys

def main():
    args = arguments.Arguments().arguments

    helpers.DEBUG_LEVEL = args.verbose
    helpers.debug(5, args)

    cfile = '/etc/rtool/rtool.yml'
    conf = config.Config(cfile, args.config)

    session_config = helpers.session_config(conf, args)
    session = api.Session(*session_config)

    service = services.get(args.service)
    executor = service.Executor(conf, args, session)
    executor.execute()
