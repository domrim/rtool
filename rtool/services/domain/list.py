from ..raw import base

class Method(base.Method):
    def service(self):
        return 'domain'

    def method(self):
        return 'domainsFind'

    def data(self):
        data = {}
        data['limit'] = 0
        data['sort'] = {"field": "domainNameUnicode", "order": "asc"}
        filter = []
        if 'contact' in self.args and self.args.contact is not None:
            filter.append(('ContactId', self.args.contact))
            filter.append(('ContactHandle', self.args.contact))
        data.update(self.filter(*filter))
        return data

    def format_default_success(self, dct):
        domains = []
        for domobj in dct['response']['data']:
            domains.append(domobj['nameUnicode'])
        return "\n".join(domains)
