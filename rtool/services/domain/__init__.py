import os
import glob
__all__ = [ os.path.basename(f)[:-3] for f in glob.glob(os.path.dirname(__file__)+'/*.py') if not os.path.basename(f).startswith('_')]

from . import *

class Executor(object):
   def __init__(self, config, args, session):
      self.config = config
      self.args = args
      self.session = session

      self.method = self.getmethod()

   def getmethod(self):
     try:
         a = globals()[self.args.method].Method
     except KeyError:
        raise NotImplementedError("method {method} not implemented yet".format(method=self.args.method))
     return a

   def execute(self):
      method = self.method(self.config, self.args, self.session)
      method.execute()
