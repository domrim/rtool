from . import list

class Method(list.Method):
    def data(self):
        data = super(Method, self).data()
        filter = []
        filter.append(('ZoneNameUnicode', self.args.domain))
        filter.append(('ZoneName', self.args.domain))
        data.update(self.filter(*filter))
        return data

    def format_default_success(self, dct):
        zones = {}
        for zoneconfigobj in dct['response']['data']:
            z = [{'type': zoneconfigobj['type'].lower()}, {'master':  zoneconfigobj['masterIp']}]
            zones[zoneconfigobj['nameUnicode']] = z
        return self.format_yaml(zones)
