from ..raw import base
import json
import sys
import pprint

class Method(base.Method):
    def service(self):
        return self.args.api_service

    def method(self):
        return self.args.api_method

    def data(self):
        return json.loads(self.get_data())

    def get_data(self):
        if self.args.json:
            return self.args.json
        else:
            return "".join(sys.stdin.readlines())

    def request(self):
        if self.args.lazy:
            result =  self.session.request(self.service(), self.method(), self.data(), self.transaction_id())
        else:
            result =  self.session.raw_request(self.service(), self.method(), self.get_data())
        if not result.ok:
            pprint.pprint(result.text)
            result.raise_for_status()
        else:
            return result.json()

    def format_default_pending(self, dct):
        return self.format_default_success(dct)
