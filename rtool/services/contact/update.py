from . import show
import sys

class Method(show.Method):
    def method(self):
        return 'contactUpdate'

    def data(self):
        try:
            c = show.Method(self.config, self.args, self.session).request()['response']
        except KeyError:
            print("contact {contact} not found".format(contact=self.args.contact.pop()))
            sys.exit(1)
        for a in [ x for x in show.list.ATTR if not x in ['type', 'name', 'organization']]:
            if a in self.args and vars(self.args)[a] is not None:
                c[a] = vars(self.args)[a]
        data = {'contact': c}
        return data

    def format_default_success(self, dct):
        return self.format_yaml(self.f_contact(dct['response']))
