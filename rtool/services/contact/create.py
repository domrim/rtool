from . import show

class Method(show.Method):
    def service(self):
        return 'domain'

    def method(self):
        return 'contactCreate'

    def data(self):
        data = {}
        data['contact'] = {}
        for a in show.list.ATTR:
            if a in self.args and vars(self.args)[a] is not None:
                data['contact'][a] = vars(self.args)[a]
            elif 'contact' in self.config and a in self.config['contact']:
                data['contact'][a] = self.config['contact'][a]
        return data
