from pprint import pprint
import sys

DEBUG_LEVEL=0

def debug(level, *messages):
   if level <= DEBUG_LEVEL:
       for message in messages:
           pprint(message)


def session_config(config, args):
    if 'url' in args and args.url is not None:
        api_url = args.url
    elif 'api' in config and 'url' in config['api']:
        api_url = config['api']['url']
    else:
        api_url = None
    if api_url is None:
        print('url argument is missing')
        sys.exit(1)

    if 'token' in args and args.token is not None:
        token = args.token
    elif 'api' in config and 'token' in config['api']:
        token = config['api']['token']
    else:
        token = None
    if token is None:
        print('token argument is missing')
        sys.exit(1)

    if 'account' in args and args.account is not None:
        account = args.account
    else:
        account = None

    return (api_url, token, account)

def prompt(question, default='yes', choices=((('yes','y'), True), (('no','n'), False)), force=None):
    c = [ y[0][0].upper() if y[0][0] == default else y[0][0] for y in choices ]
    if force is not None and force not in [z for v in choices for z in v[0]]:
        raise ValueError
    while True:
        print("{} [{}] ".format(question, "/".join(c)), end='')
        if force is None:
            choice = input().lower()
        else:
            print(force)
            choice = force
        if choice == '':
            choice = default
        a = [x[1] for x in choices if choice in x[0]]
        if len(a) == 1:
           return a[0]
        elif len(a) > 1:
           raise ValueError
