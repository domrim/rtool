from . import helpers
import yaml
import os.path
import copy

class Config(dict):
    def __init__(self, *cfiles):
        super(Config, self).__init__()

        for cfile in cfiles:
            if not os.path.isfile(cfile):
                helpers.debug(1, "config {cfile} is no file, ignoring".format(cfile=cfile))
                continue
            with open(cfile) as f:
                for y in yaml.load_all(f):
                    if y is None:
                        helpers.debug(1, "config {cfile} is empty, ignoring".format(cfile=cfile))
                        continue
                    self.merge(y)

    def merge(self, config):
        for key, value in config.items():
           if (key in self and isinstance(self[key], dict)
               and isinstance(config[key], dict)):
               self[key].update(config[key])
           else:
               self[key] = config[key]
